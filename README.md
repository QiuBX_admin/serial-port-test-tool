# 串口测试工具

#### 介绍
一款使用java Gui编写的串口测试工具
使用了RXTXcomm支持


#### 使用说明

1.  编写main方法，直接运行page类中的startui方法

#### 工具截图
 
![工具截图](https://images.gitee.com/uploads/images/2021/0708/103012_d4610332_7356136.png "屏幕截图.png")

#### 代码说明
将rxtxParallel.dll与rxtxSerial.dll拷贝进入JAVA_HOME/bin目录下
在项目中导入jar包

