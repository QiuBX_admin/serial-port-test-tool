/** @package:com.yxw.ui
 *	@author: Administrator
 *	@date: 2018年11月29日上午9:50:49
 */
package com.yxw.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DebugGraphics;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.yxw.serialportutil.SerialPortUtil;

import gnu.io.SerialPort;

/** @Classname:PAGE
 *	@Description:TODO
 *	@author: Administrator
 *	@date: 2018年11月29日上午9:50:49
 */
public class Page {
	
	JTextArea jta_rm = new JTextArea();    //信息接收区域
	JTextArea jta_sma = new JTextArea();   //信息发送区域
	
	public void startui(){
		
		JFrame jf = new JFrame("串行端口调试工具");
		
		//logo
		BufferedImage logoImage = null;  
		try {  
			logoImage = ImageIO.read(jf.getClass().getResource("/com/yxw/ui/slogo-2.png"));  
		} catch (IOException e) {  
			//TODO Auto-generated catch block  
			e.printStackTrace();  
		}
		jf.setIconImage(logoImage);
		
		//设置为南北布局(边界布局),分为东，中，西三大块功能区
		jf.setLayout(new BorderLayout());
		//宽900,高750
		jf.setSize(900,750);
		
		jf.setLocation(300,200);
		jf.setResizable(false);
		//jf.add(new JButton("东"),BorderLayout.EAST);
		//西边的区域，主要为配置项的按钮等
		jf.add(WESTJPanel(),BorderLayout.WEST);
		
		//中部的配置区域，内容为发送区和接收区两大输入框
		jf.add(CENTERJPanel(),BorderLayout.CENTER);
		
		jf.setVisible(true);
	}
	
	public JPanel WESTJPanel(){
		ClickActionUtil cau = new ClickActionUtil();
		
		JPanel jp = new JPanel();
		//左边配置项为表格布局，20行一列
		jp.setLayout(new GridLayout(20,1,5,5));
		//宽200,高900
		jp.setSize(200,900);
		
		List<String> ls = new ArrayList<String>();
		//ls.add("test111111");
		//ls.add("test222222");
		//分为南中两大块 
		//需要初始化
		SerialPortUtil spu = new SerialPortUtil();
		List<String> allPort = spu.getAllSerialPort();
		
		JComboBox<String> JComboBox_port = CreateJComboBox("端    口",allPort);
		JPanel JPanel_port = CreateJpanel("端    口", JComboBox_port);
		jp.add(JPanel_port);
		
		ls.clear();
		ls.add("50");ls.add("75");ls.add("100");ls.add("134");ls.add("150");ls.add("200");ls.add("300");ls.add("600");
		ls.add("1200");ls.add("1800");ls.add("2400");ls.add("4800");ls.add("9600");ls.add("14400");ls.add("19200");
		ls.add("38400");ls.add("56000");ls.add("57600");ls.add("76800");ls.add("115200");ls.add("128000");ls.add("256000");	
		JComboBox<String> JComboBox_btl = CreateJComboBox("波特率",ls);
		JPanel JPanel_btl = CreateJpanel("波特率", JComboBox_btl);
		jp.add(JPanel_btl);
		
		ls.clear();
		ls.add("无");
		ls.add("奇");
		ls.add("偶");
		JComboBox<String> JComboBox_jyw = CreateJComboBox("校验位",ls);
		JPanel JPanel_jyw = CreateJpanel("校验位", JComboBox_jyw);
		jp.add(JPanel_jyw);
			
		ls.clear();
		ls.add("5");
		ls.add("6");
		ls.add("7");
		ls.add("8");
		JComboBox<String> JComboBox_sjw = CreateJComboBox("数据位",ls);
		JPanel JPanel_sjw = CreateJpanel("数据位", JComboBox_sjw);
		jp.add(JPanel_sjw);

		ls.clear();
		ls.add("1");
		ls.add("1.5");
		ls.add("2");
		JComboBox<String> JComboBox_tzw = CreateJComboBox("停止位",ls);
		JPanel JPanel_tzw = CreateJpanel("停止位", JComboBox_tzw);
		jp.add(JPanel_tzw);
		
		JButton openPortButton = new JButton("打开串口");
		JButton closePortButton = new JButton("关闭串口");
		closePortButton.setEnabled(true);
		
		JPanel openPortPanel = new JPanel();
		
		closePortButton.setEnabled(false);
		
		openPortPanel.add(openPortButton);
		openPortPanel.add(closePortButton);
		openPortPanel.setPreferredSize(new Dimension(100, 50));
		jp.add(openPortPanel);
		
		//List<String> lsc = new ArrayList<String>();
		//lsc.add("ASCII");
		//lsc.add("Hex");
		JCheckBox ASCIICheckbox = CreateJCheckBox("ASCII");
		ASCIICheckbox.setSelected(true);
		
		JCheckBox checkDataCheckbox = CreateJCheckBox("校验解析");
		//JPanel ASCIIAndHexJpanel = CreateJCheckBoxPanel(ASCIICheckbox,checkDataCheckbox);
		JPanel ASCIIAndHexJpanel = CreateJCheckBoxPanel(ASCIICheckbox);
		jp.add(ASCIIAndHexJpanel);
		
		JCheckBox timeCheckbox = CreateJCheckBox("时间戳");
		JCheckBox ClearCheckbox = CreateJCheckBox("自动清空");
		JPanel timeAndClearJpanel = CreateJCheckBoxPanel(timeCheckbox,ClearCheckbox);
		jp.add(timeAndClearJpanel);
		
		JCheckBox addLineEndCheckbox = CreateJCheckBox("自动换行");
		JCheckBox showSendCheckbox = CreateJCheckBox("显示发送");
		JPanel addLineEndAndShowSendJpanel = CreateJCheckBoxPanel(addLineEndCheckbox,showSendCheckbox);
		jp.add(addLineEndAndShowSendJpanel);
		
		JButton clearReciveAreaButton = new JButton(" 清  空  ");
		JPanel clearReciveAreaPanel = new JPanel();
		clearReciveAreaPanel.add(clearReciveAreaButton);
		//clearReciveAreaPanel.setPreferredSize(new Dimension(50, 50));
		//jp.add(clearReciveAreaPanel);
		ls.clear();
		ls.add("二进制");
		ls.add("八进制");
		ls.add("十六进制");

		JComboBox<String> JComboBox_sendDataType = CreateJComboBox("",ls);
		JComboBox_sendDataType.setEnabled(false);
		JPanel JPanel_sendDataType = CreateJpanel("", JComboBox_sendDataType);
		JPanel_sendDataType.add(clearReciveAreaButton);
		jp.add(JPanel_sendDataType);
		
		JSeparator jsep = new JSeparator();
		jp.add(jsep);
		
		JPanel jp_looArea = new JPanel();
		JCheckBox loopTestCheckbox = CreateJCheckBox("循环测试");
		JTextField jtf_loopTime = new JTextField(6);
		jtf_loopTime.setText("1000");
		JLabel jp_ms = new JLabel("ms");
		jp_looArea.add(loopTestCheckbox);
		jp_looArea.add(jtf_loopTime);
		jp_looArea.add(jp_ms);
		jp.add(jp_looArea);
		
		JButton sendMessageBySelfButton = new JButton("手  动  发  送");
		JPanel sendMessageBySelfPanel = new JPanel();
		sendMessageBySelfPanel.add(sendMessageBySelfButton);
		//sendMessageBySelfPanel.setPreferredSize(new Dimension(50, 50));
		//jp.add(sendMessageBySelfPanel);
		
		JComboBox<String> JComboBox_reciveDataType = CreateJComboBox("",ls);
		JPanel JPanel_reciveDataType = CreateJpanel("", JComboBox_reciveDataType);
		JPanel_reciveDataType.add(sendMessageBySelfButton);
		jp.add(JPanel_reciveDataType);
		
		JSeparator jsep2 = new JSeparator();
		jp.add(jsep2);
		
		//计数器
		JLabel sendLable = new JLabel("发送:");
		JLabel reciveLable = new JLabel("接收:");
		JLabel sendAccountLable = new JLabel("0");
		JLabel reciveAccountLable = new JLabel("0");
		JPanel jpl_jsq = new JPanel();
		jpl_jsq.add(sendLable);
		jpl_jsq.add(sendAccountLable);
		jpl_jsq.add(reciveLable);
		jpl_jsq.add(reciveAccountLable);
		jp.add(jpl_jsq);

		
		//ls.clear();
		//ls.add("整形");
		//ls.add("浮点型");
		//ls.add("布尔型");
		//ls.add("条件语句类型");
		//ls.add("分支语句类型");
		//ls.add("计算字段");
		//ls.add("校验字段");
		//ls.add("数组字段");
		//JComboBox<String> JComboBox_xyzdlx = CreateJComboBox("协议字段类型",ls);
		//JPanel JPanel_xyzdlx = CreateJpanel("协议字段类型", JComboBox_xyzdlx);
		//jp.add(JPanel_xyzdlx);

		//ls.clear();
		//ls.add("RS232");
		//ls.add("RS422");
		//ls.add("RS422>125kbps");
		//JComboBox<String> JComboBox_jklx = CreateJComboBox("接口类型",ls);
		//JPanel JPanel_jklx = CreateJpanel("接口类型", JComboBox_jklx);
		//jp.add(JPanel_jklx);
		
		ls.clear();
		ls.add("重传测试");
		//JComboBox<String> JComboBox_yccs = CreateJComboBox("异常测试",ls);
		//JPanel JPanel_yccs = CreateJpanel("异常测试", JComboBox_yccs);
		//jp.add(JPanel_yccs);
		
		ls.clear();
		ls.add("真实性测试");
		//JComboBox<String> JComboBox_mngj = CreateJComboBox("模拟攻击",ls);
		//JPanel JPanel_mngj = CreateJpanel("模拟攻击", JComboBox_mngj);
		//jp.add(JPanel_mngj);
		
		//宽400,高1000
		jp.setPreferredSize(new Dimension(200, 900));

		//给按钮绑定点击事件
		//打开串口以及监听的点击事件
		SerialPort serialPort = cau.openPortAction(openPortButton,closePortButton,JComboBox_port,JComboBox_btl,JComboBox_jyw,JComboBox_sjw,JComboBox_tzw
				,ASCIICheckbox, checkDataCheckbox, timeCheckbox,
				  addLineEndCheckbox,JComboBox_sendDataType
				);
		
		cau.setJta_rm(jta_rm);
		cau.setJta_sma(jta_sma);
		cau.setReciveAccountLable(reciveAccountLable);
		cau.setSendAccountLable(sendAccountLable);
		cau.setSerialPort(serialPort);
		cau.setJComboBox_reciveDataType(JComboBox_reciveDataType);
		//关闭串口
		cau.closePortAction(openPortButton, closePortButton, JComboBox_port, JComboBox_btl, JComboBox_jyw, JComboBox_sjw, JComboBox_tzw);
		
		//发送事件
		cau.senfMeaasgeAction(sendMessageBySelfButton,jta_sma,JComboBox_port, ASCIICheckbox, checkDataCheckbox, timeCheckbox, ClearCheckbox,
				addLineEndCheckbox, showSendCheckbox , JComboBox_sendDataType, loopTestCheckbox, jtf_loopTime
				);
		
		//ascii逻辑
		cau.ASCIICheckBox(ASCIICheckbox, JComboBox_sendDataType);
		
		//清空接收区事件
		cau.clearReaciveArea(clearReciveAreaButton, jta_rm);
		
		cau.loopSendMessage(loopTestCheckbox, jtf_loopTime, JComboBox_port, ASCIICheckbox,checkDataCheckbox, timeCheckbox, ClearCheckbox, addLineEndCheckbox, showSendCheckbox);
		
		return jp;
	}
	
	public JPanel CENTERJPanel(){
		//中部采用表格布局，两行一列，一行为接收区，一行为发送区
		JPanel jp = new JPanel();
		
		jp.setLayout(new GridLayout(2,1,10,10));
		//宽750,高900
		jp.setSize(750,900);
		
		//分为两行
		//第一行为接收区
		//jta_rm.set
		jp.add(recivemessage());
		//第二行发送区
		jp.add(sendmessagearea());
		
		return jp;
	}
	
	public JScrollPane sendmessagearea(){
		JPanel jp = new JPanel();
		jta_sma.setEditable(true);
		JScrollPane jsp = new JScrollPane(jta_sma);
		jsp.setSize(750,300);
		jp.add(jsp);
		jp.setPreferredSize(new Dimension(750,300));
		return jsp;
	}
	
	public JScrollPane recivemessage(){
		JPanel jp = new JPanel();
		jta_rm.setEditable(false);
		JScrollPane jsp = new JScrollPane(jta_rm);
		jsp.setSize(750,600);
		jp.add(jsp);
		jp.setPreferredSize(new Dimension(750,600));
		return jsp;
	}
	
	public JComboBox<String> CreateJComboBox(String Labelname,List<String> item){
		JComboBox<String> comboBox=new JComboBox<String>();
		for(String i : item){
			comboBox.addItem(i);
		}
		return comboBox;
	}
	
	public JPanel CreateJpanel(String Labelname,Object javaswing){
		JPanel jp = new JPanel();
		JLabel label=new JLabel(Labelname);
		jp.add(label);
		jp.add((Component)javaswing);
		return jp;
	}
	
	public JPanel CreateJCheckBoxPanel(JCheckBox JCheckBox1,JCheckBox JCheckBox2){
		JPanel jp = new JPanel();
		jp.setLayout(new GridLayout(1,2,1,1));
		jp.add(JCheckBox1);
		jp.add(JCheckBox2);
		return jp;
	}
	
	public JPanel CreateJCheckBoxPanel(JCheckBox JCheckBox1){
		JPanel jp = new JPanel();
		jp.setLayout(new GridLayout(1,2,1,1));
		jp.add(JCheckBox1);
		//jp.add(JCheckBox2);
		return jp;
	}
	
	public JCheckBox CreateJCheckBox(String name){
		JCheckBox checkBox=new JCheckBox(name);
		return checkBox;
	}
	
}
