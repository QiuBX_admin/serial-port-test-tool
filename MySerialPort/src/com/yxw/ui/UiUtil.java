/** @package:com.yxw.ui
 *	@author: Administrator
 *	@date: 2018年11月29日上午9:46:25
 */
package com.yxw.ui;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/** @Classname:ButtonUtil
 *	@Description:TODO
 *	@author: Administrator
 *	@date: 2018年11月29日上午9:46:25
 */
public class UiUtil {
	public JButton createJBotton(String buttonname){
		return new JButton(buttonname);
	}
	
	public JScrollPane createJSCrollPane(){
		return new JScrollPane();
	}
	
	public JPanel createJPanel(){
		return new JPanel();
	}
	
	public JTextField ReciveText(){
		return new JTextField();
	}
}
