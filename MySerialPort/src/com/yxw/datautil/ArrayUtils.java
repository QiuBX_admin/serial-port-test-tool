/** @package:com.yxw.datautil
 *	@author: Administrator
 *	@date: 2018年11月30日上午9:46:13
 */
package com.yxw.datautil;

/** @Classname:ArrayUtils
 *	@Description:TODO
 *	@author: Administrator
 *	@date: 2018年11月30日上午9:46:13
 */
public class ArrayUtils {
	/**
	 * 合并数组
	 * 
	 * @param firstArray
	 *            第一个数组
	 * @param secondArray
	 *            第二个数组
	 * @return 合并后的数组
	 */
	public static byte[] concat(byte[] firstArray, byte[] secondArray) {
		if (firstArray == null || secondArray == null) {
			return null;
		}
		byte[] bytes = new byte[firstArray.length + secondArray.length];
		System.arraycopy(firstArray, 0, bytes, 0, firstArray.length);
		System.arraycopy(secondArray, 0, bytes, firstArray.length, secondArray.length);
		return bytes;
	}
}
